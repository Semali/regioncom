import React from 'react';
import {render} from 'react-dom';
import MultiSelectField from './MultiSelectField.jsx';

class App extends React.Component {
	render () {
		return (
			<div className="container">
				<MultiSelectField/>
			</div>
		);
	}
}

render(<App/>, document.getElementById('app'));