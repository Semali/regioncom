import React from 'react';
import createClass from 'create-react-class';
import PropTypes from 'prop-types';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import CustomOption from './CustomOption.jsx';

var MultiSelectField = createClass({
	displayName: 'MultiSelectField',
	propTypes: {
		label: PropTypes.string,
	},

	getInitialState () {
		return {
			removeSelected: false,
			stayOpen: true,
			value: []
		};
	},

	handleSelectChange (value) {
		let check1 = value.findIndex((elem) => {return elem.value === 'all'}) !== -1 ? true : false;
		let check2 = this.state.value.findIndex((elem) => {return elem.value === 'all'}) !== -1 ? true : false

		if (check1 && !check2) {
			let last = { value }.value.pop();
			value = [last];
		}
		if (check1 && check2) {
			let first = { value }.value.shift();
			value = { value }.value;
		}
		this.setState({ value });
	},
	
	render () {
		const { stayOpen, value } = this.state;

		// query simulation
		const getOptions = (input, callback) => {
			setTimeout(() => {
				callback(null, {
					options: [
						{ label: 'Все', value: 'all' },
						{ label: 'Chocolate', value: 'chocolate', disabled: false },
						{ label: 'Vanilla', value: 'vanilla', disabled: false },
						{ label: 'Strawberry', value: 'strawberry', disabled: false },
						{ label: 'Caramel', value: 'caramel', disabled: false },
						{ label: 'Cookies and Cream', value: 'cookiescream', disabled: false },
						{ label: 'Peppermint', value: 'peppermint', disabled: false }
					],
					complete: true
				});
			}, 500);
		};

		let boxStyle = {maxWidth: 500};
		
		return (
			<div className="section">
				<Select.Async
					wrapperStyle={boxStyle}
					optionComponent={CustomOption}
					loadOptions = {getOptions}
					closeOnSelect = {!stayOpen}
					multi
					onChange = {this.handleSelectChange}
					placeholder = "Выберите значение"
					noResultsText = "Совпадений не найдено"
              		removeSelected = {this.state.removeSelected}
					value = {value}
				/>
			</div>
		);
	}
});

module.exports = MultiSelectField;